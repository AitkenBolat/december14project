package level1;
public class Animals implements Basics {
    private boolean notPlants = true;

    public boolean isNotPlants() {
        return notPlants;
    }

    public void setNotPlants(boolean notPlants) {
        this.notPlants = notPlants;
    }

    @Override
    public void hasMultiplied() {
        System.out.println("This animal has multiplied");
    }
    @Override
    public  void move(){
        System.out.println("This animal moved");
    }
    @Override
    public void eat(){
        System.out.println("This animal ate");
    }
    @Override
    public void breath(){
        System.out.println("This animal breathed");
    }
}
