package level1.level2;
import level1.*;
public class Invertebrates extends Animals implements Deeper2{
    private boolean hasVertebrae = false;

    public boolean isHasVertebrae() {
        return hasVertebrae;
    }

    public void setHasVertebrae(boolean hasVertebrae) {
        this.hasVertebrae = hasVertebrae;
    }
    @Override
    public void moveSlow(){
        System.out.println("This animal moves slowly");
    }
    @Override
    public void digUnderground(){
        System.out.println("this animal can move underground");
    }

}
