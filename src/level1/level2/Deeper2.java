package level1.level2;

public interface Deeper2 {
    public void digUnderground();
    public void moveSlow();
}
