package level1.level2.level3;
import level1.level2.*;
import java.util.ArrayList;

public class WarmBlooded extends Vertebrates implements Deepest1{
    private ArrayList<String> mammals = new ArrayList<>();
    private ArrayList<String> birds = new ArrayList<>();

    public ArrayList<String> getMammals() {
        return mammals;
    }

    public void setMammals(ArrayList<String> mammals) {
        this.mammals = mammals;
    }

    public ArrayList<String> getBirds() {
        return birds;
    }

    public void setBirds(ArrayList<String> birds) {
        this.birds = birds;
    }
    @Override
    public void canProduceBodyHeat(){
        System.out.println("this animal can produce body heat");
    }
    @Override
    public  void largeAnimal(){
        System.out.println("this animal is large");
    }

    public WarmBlooded(ArrayList<String> mammals, ArrayList<String> birds) {
        this.mammals = mammals;
        this.birds = birds;
    }
}
