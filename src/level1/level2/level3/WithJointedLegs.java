package level1.level2.level3;

import java.util.ArrayList;
import level1.level2.*;

public class WithJointedLegs extends Invertebrates implements Deepest2,Deepest3{
    private ArrayList<String> threePairs = new ArrayList<>();
    private ArrayList<String> moreThanThree = new ArrayList<>();

    public ArrayList<String> getThreePairs() {
        return threePairs;
    }

    public void setThreePairs(ArrayList<String> threePairs) {
        this.threePairs = threePairs;
    }

    public ArrayList<String> getMoreThanThree() {
        return moreThanThree;
    }

    public void setMoreThanThree(ArrayList<String> moreThanThree) {
        this.moreThanThree = moreThanThree;
    }

    @Override
    public void poison() {
        System.out.println("This animal has poisoned you");
    }

    @Override
    public void lotsOfChildren() {
        System.out.println("this animal has produced a lot of children");
    }
    @Override
    public void madeColony(){
        System.out.println("this animal made a colony");
    }

    public WithJointedLegs(ArrayList<String> threePairs, ArrayList<String> moreThanThree) {
        this.threePairs = threePairs;
        this.moreThanThree = moreThanThree;
    }
}
