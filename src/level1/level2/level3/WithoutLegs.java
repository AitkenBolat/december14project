package level1.level2.level3;

import java.util.ArrayList;
import level1.level2.*;
public class WithoutLegs extends Invertebrates implements Deepest4,Deepest2{
    private ArrayList<String> wormLike = new ArrayList<>();
    private ArrayList<String> notWormLike = new ArrayList<>();



    public ArrayList<String> getWormLike() {
        return wormLike;
    }

    public void setWormLike(ArrayList<String> wormLike) {
        this.wormLike = wormLike;
    }

    public ArrayList<String> getNotWormLike() {
        return notWormLike;
    }

    public void setNotWormLike(ArrayList<String> notWormLike) {
        this.notWormLike = notWormLike;
    }

    @Override
    public void parasite() {
        System.out.println("this animal parasitizes on you");
    }

    @Override
    public void poison() {
        System.out.println("this animal poisoned you");
    }

    @Override
    public void lotsOfChildren() {
        System.out.println("this animal produced a lot of children");
    }

    public WithoutLegs(ArrayList<String> wormLike, ArrayList<String> notWormLike) {
        this.wormLike = wormLike;
        this.notWormLike = notWormLike;
    }
}
