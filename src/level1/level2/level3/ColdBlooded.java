package level1.level2.level3;
import level1.level2.*;
import java.util.ArrayList;

public class ColdBlooded extends Vertebrates implements Deepest2{
    private ArrayList<String> fish = new ArrayList<>();
    private ArrayList<String> reptiles = new ArrayList<>();
    private ArrayList<String> amphibians = new ArrayList<>();

    public ArrayList<String> getFish() {
        return fish;
    }

    public void setFish(ArrayList<String> fish) {
        this.fish = fish;
    }

    public ArrayList<String> getReptiles() {
        return reptiles;
    }

    public void setReptiles(ArrayList<String> reptiles) {
        this.reptiles = reptiles;
    }

    public ArrayList<String> getAmphibians() {
        return amphibians;
    }

    public void setAmphibians(ArrayList<String> amphibians) {
        this.amphibians = amphibians;
    }

    @Override
    public void poison() {
        System.out.println("This animal poisoned you");
    }

    @Override
    public void lotsOfChildren() {
        System.out.println("this animal produced a lot of children");

    }

    public ColdBlooded(ArrayList<String> fish, ArrayList<String> reptiles, ArrayList<String> amphibians) {
        this.fish = fish;
        this.reptiles = reptiles;
        this.amphibians = amphibians;
    }
}
