package level1.level2;
import level1.*;
public class Vertebrates extends Animals implements Deeper1{
    private boolean hasVertebrae = true;

    public boolean isHasVertebrae() {
        return hasVertebrae;
    }

    public void setHasVertebrae(boolean hasVertebrae) {
        this.hasVertebrae = hasVertebrae;
    }
    @Override
    public void moveFast(){
        System.out.println("this animal.vertebrae can move fast");
    }
    @Override
    public void gainWeight(){
        System.out.println("this animal.vertebrae can gain a lot of weight");
    }

}
