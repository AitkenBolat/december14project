import java.util.ArrayList;
import level1.level2.level3.*;
import level1.level2.*;
import level1.*;
public class Main {
    public static void main(String[] args) {
        ArrayList<String> mammals = new ArrayList<>();
        ArrayList<String> birds = new ArrayList<>();
        mammals.add("Bear");
        mammals.add("Tiger");
        birds.add("ostrich");
        birds.add("eagle");
        WarmBlooded creature1 = new WarmBlooded(mammals,birds);
        creature1.hasMultiplied(); // from basics interface that was inherited from Animals class
        creature1.gainWeight(); // from deeper1 interface that was inherited from vertebrates clas
        creature1.canProduceBodyHeat(); // was implemented in warmBlooded class


    }
}